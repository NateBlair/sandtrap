﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

	public int rows;
	public int columns;

	public GameObject[] floorTiles;
	public GameObject leftOuterWallTile;
	public GameObject rightOuterWallTile;
	public GameObject upperOuterWallTile;
	public GameObject lowerOuterWallTile;
	public GameObject lowerLeftCornerOuterWallTile;
	public GameObject lowerRightCornerOuterWallTile;
	public GameObject upperLeftCornerOuterWallTile;
	public GameObject upperRightCornerOuterWallTile;

	public GameObject[] shrubs;

	private Transform boardHolder;
	private List <Vector2> gridPosition = new List <Vector2> ();

	void InitialiseList ()
	{
		for(int x = 0; x < columns; x++)
		{
			for(int y = 0; y < rows; y++)
			{
				gridPosition.Add (new Vector2(x, y));
			}
		}
	}

	void BoardSetup () {
		boardHolder = new GameObject ("Board").transform;

		GameObject tile = null;
		for (int x=-1; x < columns + 1; x++) {
			for (int y=-1; y < rows + 1; y++) {
				tile = floorTiles[Random.Range (0, floorTiles.Length)];
		
				// Check for if it is an outer wall tile that is needed, and if so which one
				if(x == -1 || x == columns || y == -1 || y == rows) {

					// Outerwall generation
					if(x == -1) {
						if (y == -1)
							tile = lowerLeftCornerOuterWallTile;
						else if (y == rows)
							tile = upperLeftCornerOuterWallTile;
						else 
							tile = leftOuterWallTile;
					} else if (x == columns) {
						if (y == -1)
							tile = lowerRightCornerOuterWallTile;
						else if (y == rows)
							tile = upperRightCornerOuterWallTile;
						else 
							tile = rightOuterWallTile;
					} else if (y == -1) {
						tile = lowerOuterWallTile;
					} else {
						tile = upperOuterWallTile;
					}
				}

				GameObject instance = Instantiate(tile, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
			
				instance.transform.SetParent(boardHolder);
			}
		}
	}

	private void InstantiateTile(GameObject tile, int x, int y) {
		GameObject instance = Instantiate(tile, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
		
		instance.transform.SetParent(boardHolder);
	}

	Vector2 RandomPosition (){
		int randomIndex = Random.Range (0, gridPosition.Count);

		Vector3 randomPosition = gridPosition [randomIndex];

		gridPosition.RemoveAt (randomIndex);

		return randomPosition;
	}

	public void SetupScene () {
		BoardSetup ();

		InitialiseList ();
	}
}
