﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class OasisManager : MonoBehaviour {

	private int rows;
	private int columns;

	private int numberOfShrubs = 10;

	private int waterPoolW = 3;
	private int waterPoolH = 3;

	private List<GameObject>[,] oasis;

	private List <Vector2> gridPosition = new List <Vector2> ();

	public GameObject[] bushes;
	public GameObject[] grass;
	public GameObject waterMain;
	public GameObject waterLeft;
	public GameObject waterRight;
	public GameObject waterUpper;
	public GameObject waterLower;
	public GameObject waterLowerLeft;
	public GameObject waterLowerRight;
	public GameObject waterUpperLeft;
	public GameObject waterUpperRight;

	void InitialiseList ()
	{
		for(int x = 0; x < columns; x++)
		{
			for(int y = 0; y < rows; y++)
			{
				gridPosition.Add (new Vector2(x, y));
				oasis[x, y] = new List<GameObject> ();
			}
		}
	}
	
	void BoardSetup () {
		GameObject tile = null;
		for (int x=-0; x < columns ; x++) {
			for (int y=-0; y < rows ; y++) {
				int perimeterWidth = (rows - waterPoolW) / 2;
				int perimeterHeight = (rows - waterPoolH) / 2;

				tile = grass[Random.Range (0, grass.Length)];

				// Check if we are at the water generation point
				if ((x >= perimeterWidth) && (x < (rows - perimeterWidth))|| (y >= perimeterHeight) && (y < (rows - perimeterHeight))) {
					if(x == perimeterWidth) {
						if (y == perimeterHeight)
							tile = waterLowerLeft;
						else if (y == (rows - perimeterHeight - 1))
							tile = waterUpperLeft;
						else 
							tile = waterLeft;
					} else if (x == (columns - perimeterWidth - 1)) {
						if (y == perimeterHeight)
							tile = waterLowerRight;
						else if (y == (rows - perimeterHeight - 1))
							tile = waterUpperRight;
						else 
							tile = waterRight;
					} else if (y == perimeterHeight) {
						tile = waterLower;
					} else {
						tile = waterUpper;
					}
				}
			
				oasis[x, y].Add(tile);

			}
		}

		// Shrub generation
		PlaceShrubs ();

	}
	
	Vector2 RandomPosition (){
		int randomIndex = Random.Range (0, gridPosition.Count);
		
		Vector3 randomPosition = gridPosition [randomIndex];
		
		gridPosition.RemoveAt (randomIndex);
		
		return randomPosition;
	}

	private void PlaceShrubs () {
	
		for (int count = 0; count <= numberOfShrubs; count++) {
			GameObject shrub = bushes[Random.Range (0, bushes.Length)];
			Vector2 randomPosition = RandomPosition ();

			oasis[(int)randomPosition.x, (int)randomPosition.y].Add (shrub);
		}
	}
	
	public List<GameObject>[,] SetupOasis (int width, int height) {
		rows = width;
		columns = height;

		InitialiseList ();

		BoardSetup ();

		return oasis;
	}
}
