﻿using UnityEngine;
using System.Collections;

public class WaterMeter : MonoBehaviour {

	public Sprite zeroWaterLevel;
	public Sprite oneWaterLevel;
	public Sprite twoWaterLevel;
	public Sprite threeWaterLevel;
	public Sprite fourWaterLevel;
	public Sprite fiveWaterLevel;

	public int maxWaterLevel;

	private SpriteRenderer spriteRenderer;
	// Use this for initialization
	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	public void updateWaterSprite(int waterLevel){

		if(waterLevel >= maxWaterLevel) {
			spriteRenderer.sprite = fiveWaterLevel;
		} else if(waterLevel == 4) {
			spriteRenderer.sprite = fourWaterLevel;
		} else if(waterLevel == 3) {
			spriteRenderer.sprite = threeWaterLevel;
		} else if(waterLevel == 2) {
			spriteRenderer.sprite = twoWaterLevel;
		} else if(waterLevel == 1) {
			spriteRenderer.sprite = oneWaterLevel;
		} else {
			spriteRenderer.sprite = zeroWaterLevel;
		}
	}
}
