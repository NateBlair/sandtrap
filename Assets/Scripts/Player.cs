﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;	//Allows us to use UI.

//Player inherits from MovingObject, our base class for objects that can move, Enemy also inherits from this.
public class Player : MonoBehaviour
{
	public float minMoveValue = 0.1f;
	public float moveSpeed = 6.0f; //Move speed value in all directions. Value is later multiplied by below variable; pixels per unit.
	private Vector2[] speed = new Vector2[4];	
	private Animator animator;					//Used to store a reference to the Player's animator component.
	private Vector2 touchOrigin = -Vector2.one;	//Used to store location of screen touch origin for mobile controls.
	private Rigidbody2D rgb2d;
	private float timeSinceLastUpdate = 0f;
	private bool touchingWater = false;

	public WaterMeter waterMeter;
	private int waterLevel;
	public int initWaterLevel;
	public int waterLossPerTic;
	public int secondsBeforeWaterLoss;

	public int initWaterBottleLevel;
	public int waterBottleLoss;
	private int waterBottleLevel;
	public WaterBottle waterBottle;

	void Start ()
	{
		//Get a component reference to the Player's animator component
		animator = GetComponent<Animator>();

		rgb2d = GetComponent<Rigidbody2D> ();

		// Positive y direction speed
		speed[0] = new Vector2(0, moveSpeed);

		// Negative y direction speed
		speed[1] = new Vector2(0, -moveSpeed);

		// Positive x direction speed
		speed[2] = new Vector2(moveSpeed, 0);

		// Negative x direction speed
		speed[3] = new Vector2(-moveSpeed, 0);

		waterLevel = initWaterLevel;

		waterBottleLevel = -1;
	}

	public void loseWater (int waterLoss) {
		waterLevel -= waterLoss;

		if (waterLevel <= 0) {
			GameManager.instance.GameOver();
		}
		waterMeter.updateWaterSprite (waterLevel);
	}
	
	public void gainWater (int waterGain) {
		waterLevel += waterGain;
		
		waterMeter.updateWaterSprite (waterLevel);
	}

	public void fillWater () {
		waterLevel = initWaterLevel;
		waterMeter.updateWaterSprite (waterLevel);

		if (waterBottleLevel != -1) {
			waterBottleLevel = initWaterBottleLevel;
			waterBottle.updateWaterSprite (waterBottleLevel);
		}
	}

	private void getBottle() {
		waterBottleLevel = 0;

		waterBottle.updateWaterSprite (waterBottleLevel);
	}

	private void drinkWater(int waterLoss) {
		if (waterBottleLevel > 0) {
			waterBottleLevel -= waterLoss;
			gainWater (waterLoss);

			waterBottle.updateWaterSprite (waterBottleLevel);
		}
	}
	
	private void Update ()
	{

		//Check if we are running either in the Unity editor or in a standalone build.
		#if UNITY_STANDALONE || UNITY_WEBPLAYER

		if(Input.GetKeyDown ("f")) {
			if (waterBottleLevel != -1) {
				drinkWater(waterBottleLoss);
			}
		}

		//Horizontal movement.
		if (Input.GetAxis("Horizontal") > minMoveValue) {
			// GetComponent<Rigidbody2D>().AddForce(new Vector2(speed,0) * Time.deltaTime);
			Vector2 velocity = new Vector2(0,0);
			rgb2d.MovePosition(rgb2d.position + speed[2] * Time.deltaTime);
		}
		else if (Input.GetAxis("Horizontal") < -minMoveValue) {
			// GetComponent<Rigidbody2D>().AddForce(new Vector2(-speed,0) * Time.deltaTime);
			rgb2d.MovePosition(rgb2d.position + speed[3] * Time.deltaTime);

		}
		
		//Vertical movement.
		if (Input.GetAxis("Vertical") > minMoveValue) {
			rgb2d.MovePosition(rgb2d.position + speed[0] * Time.deltaTime);
		}
		else if (Input.GetAxis("Vertical") < -minMoveValue) {
			rgb2d.MovePosition(rgb2d.position + speed[1] * Time.deltaTime);
		}
		//Check if we are running on iOS, Android, Windows Phone 8 or Unity iPhone
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		
		//Check if Input has registered more than zero touches
		if (Input.touchCount > 0)
		{
			//Store the first touch detected.
			Touch myTouch = Input.touches[0];
			
			//Check if the phase of that touch equals Began
			if (myTouch.phase == TouchPhase.Began)
			{
				//If so, set touchOrigin to the position of that touch
				touchOrigin = myTouch.position;
			}
			
			//If the touch phase is not Began, and instead is equal to Ended and the x of touchOrigin is greater or equal to zero:
			else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
			{
				//Set touchEnd to equal the position of this touch
				Vector2 touchEnd = myTouch.position;
				
				//Calculate the difference between the beginning and end of the touch on the x axis.
				float x = touchEnd.x - touchOrigin.x;
				
				//Calculate the difference between the beginning and end of the touch on the y axis.
				float y = touchEnd.y - touchOrigin.y;
				
				//Set touchOrigin.x to -1 so that our else if statement will evaluate false and not repeat immediately.
				touchOrigin.x = -1;
				
				//Check if the difference along the x axis is greater than the difference along the y axis.
				if (Mathf.Abs(x) > Mathf.Abs(y))
					//If x is greater than zero, set horizontal to 1, otherwise set it to -1
					horizontal = x > 0 ? 1 : -1;
				else
					//If y is greater than zero, set horizontal to 1, otherwise set it to -1
					vertical = y > 0 ? 1 : -1;
			}
		}
		
		#endif //End of mobile platform dependendent compilation section started above with #elif

		timeSinceLastUpdate += Time.deltaTime;

		if (timeSinceLastUpdate >= secondsBeforeWaterLoss) {
			loseWater (waterLossPerTic);
			timeSinceLastUpdate = 0f;
		}
	}

	void OnCollisionStay2D (Collision2D other) {
	
		if (Input.GetKey ("x")) {
			if (other.collider.tag == "Water") {
				fillWater();
			} else if(other.collider.tag == "Bottle") {
				getBottle();
				Destroy (other.gameObject);
			}
		}
	}
}